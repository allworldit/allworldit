Perl Language
==============

Coding guideline

## 1. Maximum Line Length

132 characters.


## 2. Whitespaces

No whitespaces at the end of the line.

No mixed space+tab for indentation.

Empty lines must be empty, no whitespaces.


## 3. Identation

Tab indentation is used, with the tab width being 4 spaces.

Continuation lines are indented twice...
if (joesoap &&
		test) {
}

print "hi there ".
		"joe soap\n";

Split up lines are idented once...
if (
	joesoap && test
) {
}

sprintf("%s",
	$test
);

$test = '
	test
';

## 4. Functions

3 empty lines between functions.

2 empty lines between function parameters and code.



CASE

##
Object properties are in lowercase

##
Object settings are in lowercase

##
Datastructure hashes are in camelcase with first character capatilized and abbreviations like SQL in caps.

##
Data structures being passed between applications or subsystems may require lowercasing attribute names


## 99. End Of File

To make life easier with tabs, files must contain

'''text
# vim: ts=4
'''

as the last line.

