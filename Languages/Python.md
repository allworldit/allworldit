PHP Language
==============

Coding guideline


## 1. Directory layout

To ensure all our repositories follow the same layout, source code should be stored in the `src/` folder.

Tests should either be setup within the `src/` folder or within a dedicated `tests/` folder.


## 2. Styling

Code must adhere to [PEP 8 -- Style Guide for Python Code](https://www.python.org/dev/peps/pep-0008/).


## 3. Linting

Linting should be setup to run automatically during tests.

See [Example](#5.1. Code coverage and linting)


## 4. Code coverage

Code coverage should be setup to ensure tests run through as much code as possible.

See [Example](#5.1. Code coverage and linting)


## 5. Examples

### 5.1. Code coverage and linting

Here is an example of the `setup.py` file...
```python
""" Example setup.py """
from setuptools import setup, find_packages

setup(
    name='Example',
    version='0.1',
    packages=find_packages(where='src'),
    package_dir={'': 'src'},
    setup_requires=[
        'pytest-runner',
        'pytest-pylint',
        'pytest-cov',
    ],
    tests_require=[
        'pytest',
        'pylint',
    ],
)
```

Here is the associated `setup.cfg` file...
```
[aliases]
test=pytest

[tool:pytest]
addopts = --pylint --cov=src/ tests/
```

Here is an example of a `.coveragerc` file...
```
[run]
branch = True

[report]
# Regexes for lines to exclude from consideration
exclude_lines =
    # Have to re-enable the standard pragma
    pragma: no cover

    # Don't complain about missing debug-only code:
    def __repr__
    if self\.debug

    # Don't complain if tests don't hit defensive assertion code:
    raise AssertionError
    raise NotImplementedError

    # Don't complain if non-runnable code isn't run:
    if 0:
    if __name__ == .__main__.:
```

