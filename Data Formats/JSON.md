# AWIT JSON Format Standard



# HTTP Methods

Make sure during implementation you read [RFC7231](https://tools.ietf.org/html/rfc7231).


## POST
The POST method is utilized to **create** new resources. POST to a parent resource must associate the new resource with the parent,
assigning an ID (new resource URI), etc.

On successful creation, return HTTP status 201, returning a Location header with a link to the newly-created resource with the 201
HTTP status.

### Return Codes
* 201 (Created)
  * 'Location' header with link to /record/{id} containing new resource ID.
* 404 (Not Found)
* 409 (Conflict)
  * If resource already exists.

### Examples
* POST http://www.example.com/customers
* POST http://www.example.com/customers/12345/orders


## GET
The GET method is used to **read** a resource. In a non-error path, GET returns a HTTP status of 200 (OK). In an error case,
it most often returns a 404 (NOT FOUND) or 400 (BAD REQUEST).

### Return Codes
* 200 (OK)
  * List of records. Use pagination, sorting and filtering to navigate big lists.
* 200 (OK)
  * Single record.
* 404 (Not Found)
  * If ID not found or invalid.

### Examples
* GET http://www.example.com/customers/12345
* GET http://www.example.com/customers/12345/orders
* GET http://www.example.com/customers/12345/orders/98765
* GET http://www.example.com/buckets/sample


## PUT
The PUT method is utilized for **replace**, PUT-ing to a known resource URI with the request body containing the replacement
resource data.

On successful replacement, return 200 (or 204 if no changes were made).

### Return Codes
* 200 (OK)
* 204 (No Content)
  * If no changes were made.
* 404 (Not Found)
  * If ID not found or invalid.

### Examples
* PUT http://www.example.com/customers/12345
* PUT http://www.example.com/customers/12345/orders/98765


## PATCH
The PATCH method is used for **modify**. The PATCH request only needs to contain the changes to the resource, not the complete
resource.

On successful modification, return 200 (or 204 if no changes were made).

### Return Codes
* 200 (OK)
* 204 (No Content)
  * If no changes were made.
* 404 (Not Found)
  * If ID not found or invalid.

### Examples
* PATCH http://www.example.com/customers/12345
* PATCH http://www.example.com/customers/12345/orders/98765


## DELETE
DELETE is used to **delete** a resource identified by a URI.

On successful deletion, return HTTP status 200 (OK).

### Return Codes
* 200 (OK)
* 404 (Not Found)
  * If ID not found or invalid.

### Examples
* DELETE http://www.example.com/customers/12345
* DELETE http://www.example.com/customers/12345/orders/98765


## OPTIONS
The OPTIONS method is used for introspection. This allows for introspection of collection or resources and methods available.

Using the OPTIONS method on the root path of the URI followed by a \* should provide a list of collections and resources available.

The 204 error code should be returned if OPTIONS is not implemented for the requested resource.

### Return Codes
* 404 (Not Found)
* 200 (OK)
* 204 (No Content)
  * If OPTIONS is not implemented for this resource.

### Examples
* OPTIONS http://www.example.com/\*
* OPTIONS http://www.example.com/customers


## PROPFIND
The PROPFIND method is used for introspection. This will allow the return of properties for a collection as a whole. It can also
be used to determine properties of arbitrary resources.

Using the PROPFIND method on the root path of the URI should return information on the API.

Using the PROPFIND method on a collection path should provide information on the record structure.

Using the PROPFIND method on a resource path should provide information on the resource structure.

The 204 error code should be returned if OPTIONS is not implemented for the requested resource.

### Return Codes
* 404 (Not Found)
* 200 (OK)
* 204 (No Content)
  * If PROPFIND is not implemented for this resource.

### Examples
* PROPFIND http://www.example.com
* PROPFIND http://www.example.com/customers



# Data Format


## The 'status' field

The purpose of this field is to signify the status of the action just performed.

|  Value  | Description | Required | Optional |
| ------ | ----------- | ------------- | ------------- |
|success|The action succeeded, the 'data' key may contain a additional information||message,data|
|fail|There was a problem with the data submitted, the 'data' field must contain additional information|message||
|error|An error occurred in processing the request, the 'data' field must contain additional information in a 'message' field and may contain an additional 'data:code' and 'data:errors' (array) fields|message|data:code,data:errors|

### Example success

For a brief status message one could use...
```jsonnet
{
  status: "success"
}
```

For a full status message one could use...
```jsonnet
{
  status: "success",
  message: "Record 17 deleted"
}
```

For a successful status which returns records...
```jsonnet
{
  status: "success",
  data: {
    records: [
      { id: 1, subject: "hi" },
      { id: 2, subject: "bye" }
    ]
  }
}
```

For a successful status which returns a single result...
```jsonnet
{
  status: "success",
  data: {
    record: {
      username: 1,
      firstname: 2
    }
  }
}
```

### Example for a fail

Failures must have a mesasge field...
```jsonnet
{
  status: "fail",
  message: "System failure, please try again later"
}
```

Or it can be verbose...
```jsonnet
{
  status: "fail",
  message: "System failure, please try again later",
  data: {
    code: 17,
    errors: [
      "Contacting remote server failed",
      "Server: xyz, Port: 123, Error: Connection reset by peer"
    ]
  }
}
```

### Example for a error

An error can be brief...
```jsonnet
{
  status: "error",
  message: "The contents of the 'subject' field is invalid"
}
```

Or it can be verbose...
```jsonnet
{
  status: "error",
  message: "Input failed validation",
  data: {
    code: 17,
    errors: [
      "Field 'title' is invalid",
      "Field 'subject' is blank"
    ]
  }
}
```


## The 'message' key

The purpose of this key is to give a quick status message as to the result of the action. This is optional for success and mandatory for failurs and errors.

The format of this keys data is plain text.


## The 'data' field

This field contains result data. Either a 'result' key or a 'records' key.

See the 'status' field definition above for examples.



# Examples

## Introspection examples

### API information using /

Using the PROPFIND method response to http://www.example.com may look like this...
```jsonnet
{
  "status": "success",
  "data": {
    "title": "Issue Tracker App",
    "description": "This is a sample server for issue tracking.",
    "termsOfService": "http://www.example.com/terms",
    "contact": {
      "name": "API Support",
      "url": "http://www.example.com/contact",
      "email": "support@example.com"
    },
    "version": "0.0.1"
  }
}
```

### Endpoint list using path /*

An OPTIONS method response to http://www.example.com/\* may look like this...
```jsonnet
{
  "status": "success",
  "data": {
    "collections": [
      "/issues"
    ],
    "resources": [
      "/system/hostname"
    ]
  }
}
```

### Collection information

A OPTIONS method response to http://www.example.com/issues may look like this...
```jsonnet
{
  "status": "success",
  "data": {
    "result": {
      "methods": {
        "POST": {
          "description": "Create an issue",
          "path": "/issues",
          "payload": {
            "title": {
              "type": "string",
              "description": "Issue title.",
              "required": true
            },
            "body": {
              "type": "string",
              "description": "Issue body."
            },
            "assignee": {
              "type": "string",
              "description" "Login for the user that this issue should be assigned to."
            },
            "milestone": {
              "type": "number",
              "description": "Milestone to associate this issue with."
            },
            "labels": {
              "type": ["array","string"],
              "description": "Labels to associate with this issue."
            }
          },
          "returns": {
            "status": {
              "type": "status",
              "description": "Result of request"
            },
            "data": {
              "type": "hash",
              "description": "Hash containing result of action, returned only on success",
              "children": {
                "record": {
                  "type": "hash",
                  "description": "Record hash"
                }
              }
            }
          },
          "example": {
            "title": "Found a bug",
            "body": "I'm having a problem with this.",
            "assignee": "octocat",
            "milestone": 1,
            "labels": [
              "Label1",
              "Label2"
            ]
          }
        },
        "GET": {
          "description": "Retrieve issue(s)",
          "path": ["/issues","/issues/:record_id"],
          "url": {
            ":record_id": {
              "type": "integer",
              "description": "Record ID",
            }
          },
          "returns": {
            "status": {
              "type": "status",
              "description": "Result of request"
            },
            "data": {
              "type": "hash",
              "description": "Hash containing result of action, returned only on success",
              "children": {
                "record": {
                  "type": "hash",
                  "description": "Record hash, returned for a single record request only"
                },
                "records": {
                  "type": "array",
                  "description": "Array of records, returned for a multi-record request only"
                }
              }
            }
          }
        },
        "DELETE": {
          "description": "Delete an item",
          "path": "/issues/:record_id",
          "url": {
            ":record_id": {
              "type": "integer",
              "description": "Record ID",
              "required": true
            }
          },
          "returns": {
            "status": {
              "type": "status",
              "description": "Result of request"
            }
          }
        },
        "PATCH": {
          "description": "Change an issue",
          "path": "/issues/:record_id",
          "url": {
            ":record_id": {
              "type": "integer",
              "description": "Record ID",
              "required": true
            }
          },
          "payload": {
            "title": {
              "type": "string",
              "description": "Issue title.",
              "required": true
            },
            "body": {
              "type": "string",
              "description": "Issue body."
            },
            "assignee": {
              "type": "string",
              "description" "Login for the user that this issue should be assigned to."
            },
            "milestone": {
              "type": "number",
              "description": "Milestone to associate this issue with."
            },
            "labels": {
              "type": ["array","string"],
              "description": "Labels to associate with this issue."
            }
          },
          "returns": {
            "status": {
              "type": "status",
              "description": "Result of request"
            },
            "data": {
              "type": "hash",
              "description": "Hash containing result of action, returned only on success",
              "children": {
                "record": {
                  "type": "hash",
                  "description": "Record hash"
                }
              }
            }
          }
        },
        "PUT": {
          "description": "Replace an issue",
          "path": "/issues/:record_id",
          "url": {
            ":record_id": {
              "type": "integer",
              "description": "Record ID",
              "required": true
            }
          },
          "payload": {
            "title": {
              "type": "string",
              "description": "Issue title.",
              "required": true
            },
            "body": {
              "type": "string",
              "description": "Issue body."
            },
            "assignee": {
              "type": "string",
              "description" "Login for the user that this issue should be assigned to."
            },
            "milestone": {
              "type": "number",
              "description": "Milestone to associate this issue with."
            },
            "labels": {
              "type": ["array","string"],
              "description": "Labels to associate with this issue."
            }
          },
          "returns": {
            "status": {
              "type": "status",
              "description": "Result of request"
            },
            "data": {
              "type": "hash",
              "description": "Hash containing result of action, returned only on success",
              "children": {
                "record": {
                  "type": "hash",
                  "description": "Record hash"
                }
              }
            }
          }
        },
        "OPTIONS": {
          "description": "Collection method information",
          "path": "/issues",
          },
          "returns": {
            "status": {
              "type": "status",
              "description": "Result of request"
            },
            "data": {
              "type": "hash",
              "description": "Hash containing result of action, returned only on success",
              "children": {
                "methods": {
                  "type": "hash",
                  "description": "Method information hash"
                }
              }
            }
          }
        },
        "PROPFIND": {
          "description": "Collection information",
          "path": "/issues",
          "returns": {
            "status": {
              "type": "status",
              "description": "Result of request"
            },
            "data": {
              "type": "hash",
              "description": "Hash containing result of action, returned only on success",
              "children": {
                "definition": {
                  "type": "hash",
                  "description": "Record definition hash"
                },
                "records": {
                  "type": "integer",
                  "description": "Number of items in collection"
                }
              }
            }
          }
        }
      }
    }
  }
}
```

### Resource information

A OPTIONS method response to http://www.example.com/system/hostname may look like this...
```jsonnet
{
  "status":"success",
  "data":{
    "methods":{
      "PATCH":{
        "returns":{
          "status":{
            "type":"status",
            "description":"Result of request"
          },
          "data":{
            "description":"Hash containing result of action, returned only on success",
            "children":{
              "resource":{
                "type":"hash",
                "description":"Modified resource hash"
              }
            },
            "type":"hash"
          }
        },
        "payload":{
          "Hostname":{
            "description":"System hostname (FQDN)",
            "type":"domain"
          }
        },
        "path":"/system/hostname"
      },
      "PUT":{
        "payload":{
          "Hostname":{
            "type":"domain",
            "description":"System hostname (FQDN)"
          }
        },
        "path":"/system/hostname",
        "returns":{
          "status":{
            "description":"Result of request",
            "type":"status"
          },
          "data":{
            "description":"Hash containing result of action, returned only on success",
            "children":{
              "resource":{
                "type":"hash",
                "description":"Replaced resource hash"
              }
            },
            "type":"hash"
          }
        }
      },
      "POST":{
        "path":"/system/hostname",
        "payload":{
          "Hostname":{
            "type":"domain",
            "required":true,
            "description":"System hostname (FQDN)"
          }
        },
        "example":{
          "Hostname":"test.example.net"
        },
        "returns":{
          "status":{
            "type":"status",
            "description":"Result of request"
          },
          "data":{
            "type":"hash",
            "description":"Hash containing result of action, returned only on success",
            "children":{
              "resource":{
                "type":"hash",
                "description":"Created record hash"
              }
            }
          }
        }
      },
      "PROPFIND":{
        "path":"/system/hostname",
        "returns":{
          "data":{
            "description":"Hash containing result of action, returned only on success",
            "children":{
              "definition":{
                "description":"Record definition hash",
                "type":"hash"
              }
            },
            "type":"hash"
          },
          "status":{
            "type":"status",
            "description":"Result of request"
          }
        }
      },
      "GET":{
        "returns":{
          "data":{
            "children":{
              "resource":{
                "type":"hash",
                "description":"Resource hash"
              }
            },
            "description":"Hash containing result of action, returned only on success",
            "type":"hash"
          },
          "status":{
            "type":"status",
            "description":"Result of request"
          }
        },
        "path":"/system/hostname"
      },
      "DELETE":{
        "returns":{
          "status":{
            "type":"status",
            "description":"Result of request"
          },
          "data":{
            "type":"hash",
            "description":"Hash containing result of action, returned only on success"
          }
        },
        "path":"/system/hostname"
      },
      "OPTIONS":{
        "path":"/system/hostname",
        "returns":{
          "status":{
            "description":"Result of request",
            "type":"status"
          },
          "data":{
            "description":"Hash containing result of action, returned only on success",
            "children":{
              "methods":{
                "type":"hash",
                "description":"Method information hash"
              }
            },
            "type":"hash"
          }
        }
      }
    }
  }
}
```

# Acknoledgements

Inspired by [RestApiTutorial.com](http://www.restapitutorial.com)

Inspired by [Swagger.io](http://swagger.io/specification)

