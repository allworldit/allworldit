Submission Policy
==============

In order to submit code and for there to be a chance these submissions are included upstream there are a few policies which need to be adhered to.

## 1. Signed-Off

1.1. All commits must include a "Signed-Off: Name Surname <Email>" tag, adding this tag signifies that...

```text
Developer's Certificate of Origin 1.1

By making a contribution to this project, I certify that:

(a) The contribution was created in whole or in part by me and I have the right
    to submit it under the open source license indicated in the file; or

(b) The contribution is based upon previous work that, to the best of my
    knowledge, is covered under an appropriate open source license and I have
    the right under that license to submit that work with modifications, whether
    created in whole or in part by me, under the same open source license
    (unless I am permitted to submit under a different license), as indicated in
    the file; or

(c) The contribution was provided directly to me by some other person who
    certified (a), (b) or (c) and I have not modified it.

(d) I understand and agree that this project and the contribution are public and
    that a record of the contribution (including all personal information I
    submit with it, including my sign-off) is maintained indefinitely and may be
    redistributed consistent with this project or the open source license(s)
    involved.
```

The text of the DCO is located in the file [Documentation/SubmittingPatches](http://git.kernel.org/?p=linux/kernel/git/torvalds/linux-2.6.git;a=blob;f=Documentation/SubmittingPatches) in the Linux kernel source tree.

## 2. License

2.1. All files must contain the relevant license block at the top with a file description line (see other files for the block to use), example...

```text
# AWIT CPM - AllWorldIT Credit Processing Module
# Copyright (c) 2013, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
```

## 3. Tabs/Spaces

3.1. All files must be tab indented unless the language prohibits this

3.2. No whitespaces at the end of the line are permitted

## 4. Line Endings

4.1. All line endings must be UNIX (\n) not DOS (\r\n) format