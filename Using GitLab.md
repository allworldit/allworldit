Contributing to a Repository
============================

The right way to contribute changes to a repository.

## Setting Up Your Git

Setup Git global settings with the below commands...
```sh
git config --global user.name "Your Name"
git config --global user.email "your@email"
git config --global user.signingkey "gpg_fingerprint"

git config --global format.signoff true
git config --global core.whitespace trailing-space,space-before-tab
git config --global color.ui true
```

## Forking

* Log into [LinuxAssist Devlabs GitLab](https://gitlab.devlabs.linuxassist.net)
* Find the project you're wanting to change, click on the repository
* Top right hand side, click "Fork"

You now have your own copy of the repository.

## Clone Locally

Clone the origin repository...

```sh
git clone ssh://git@gitlab.devlabs.linuxassist.net/upstream/repo.git
git config format.signature true
```

Fork the repository.

Add your repository remote...

```sh
git remote add $USER ssh://git@gitlab.devlabs.linuxassist.net/yourusername/repo.git
```

## Make Your Changes

We first need to branch, add hunks, review and submit.

### Branching

For each separate change you going to make you must create a branch...

```sh
git fetch origin
git checkout origin/master -b your-change-name
```

Check the commit log and try use branch names that relate to software modules or components.

You can now modify the relevant files.

#### Adding Hunk-By-Hunk

You can add hunk by hunk, or group of hungs by group of hunks using the following... (filename is optional)

```sh
git add --patch [filename]
```

Type ? at the prompt to get further help. The (e)dit option is VERY useful to pull in only changes that are surgical.

#### Removing Hunk-By-Hunk (optional)

If you mistakenly git add --patch a wrong hunk or need to ammend it use the following...

```sh
git reset --patch [filename]
```

### Committing Your Changes

The first thing you need to do is review your changes using the following command before you commit...
```sh
git diff --staged
```

If you are 100% happy, commit your chagnes using the below, making sure of course all your changes must contain the Signed-Off line...
```sh
git commit --signoff
```


### Cherry-picking Your Changes (optional)

Sometimes you may find you have a commit in another branch you wish to submit, this is very easy to do. Git has a cherry-pick command which will fetch the commit and apply it to the branch you're in.

```sh
git cherry-pick de20a8566bf7a996d4bc8bde997fc359560f2b28
```

Remember to review the number of commits in your MR. Typically there should only be 1. See the below section about "Squashing Your Commits".



## Submitting Your Changes

Be sure to read the code [Submission Policy](https://gitlab.devlabs.linuxassist.net/allworldit/allworldit/blob/master/Submission%20Policy.md)

### Push Changes

Make sure your code is based off the main repo and push your branch...

```sh
git fetch origin
git rebase origin/master
git push $USER -f
```

We use the -f option for force overwriting of your branch in your forked repository.

### Submit Upstream

Using the web interface you can then create a merge request:
* Click on your repository, click "Create Merge Request"
* Select the repository and branch on the top left and top right
* Type in a description of your changes
* Click "Submit merge request"

## Post-Review Changes

If you get comments on your code and you need to make changes to the branch merge request, its really easy to do.

### Fixing Your Code

Go through each comment one by one and commit the fixes as required.

### Squashing Your Commits

You do NOT want to submit 20,000 commits, squashing will take all the changes and squash them into 1 commit. Replace X with how many extra comments you made PLUS 1 to include your original commit.

You can also re-order commits in order to squash similar commits together.

To squash all commits since "master", use...

```sh
git fetch origin
git rebase -i origin/master
```

To squash X number of commits use the following...

```sh
git rebase -i HEAD~X
```

In all cases will be presented with a list of commits. Replace `pick` with `s` on all but your original commit. The next step will present you with a commit message with all your commit messages included for edit.

### Re-Pushing

You can now re-push your changes by forcing a ref update...

```
git fetch origin
git rebase origin/master
git push $USER -f
```

